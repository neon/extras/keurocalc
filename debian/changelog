keurocalc (1.3.0-4) unstable; urgency=medium

  * Team upload.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper-compat build dependency to 13
    - stop passing --fail-missing to dh_missing, as it is the default behaviour
  * Remove the explicit as-needed linking, as it is done by binutils now.
  * Drop the breaks/replaces for versions older than Debian Buster.
  * Update the references to the upstream Git repository.
  * Bump Standards-Version to 4.6.0, no changes required.
  * Add Rules-Requires-Root: no.
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.0
    - bump Qt packages to 5.3.0
    - drop "1" revision from the versions of KF packages
  * Backport upstream commits 8a53b199623e7f948538c4ac1bb2812a380e9507 and
    131044f534ea9e96579a982285445dc83d641cd6 to improve the AppStream XMLs;
    patches upstream_Applied-appstream-metainfo-release-update-script.patch,
    and upstream_appdata-fix-screenshot-location.patch.

 -- Pino Toscano <pino@debian.org>  Thu, 28 Oct 2021 08:34:12 +0200

keurocalc (1.3.0-3) unstable; urgency=medium

  * Team upload.
  * Move the AppStream metadata from keurocalc-data to keurocalc, as it must be
    together with the executable it represents
    - add proper breaks/replaces
  * Remove "binary package" from the short description of keurocalc, as it is
    not needed.
  * Switch from the debhelper build dependency to debhelper-compat
    - remove debian/compat
  * Bump Standards-Version to 4.4.1, no changes required.
  * Fix typo in old changelog entry.

 -- Pino Toscano <pino@debian.org>  Sun, 05 Jan 2020 13:43:43 +0100

keurocalc (1.3.0-2) unstable; urgency=medium

  * Fix "[keurocalc] Future Qt4 removal from Buster"
    - QT5 Port. (Closes: #874952)
  * New upstream release.
  * Update watch file and add signing-key.asc.
  * Use --with=kf5 in rules.
  * Update build depends as per cmake.
  * Replace uploaders with myself with permission from
    original maintainer to take over.
  * Update copyright file.
  * Bump compat to 12.
  * Bump standards to 4.4.0; No remaining changes.
  * Bump version to new version.
  * Update install files.
  * Add upstream metainfo file.
  * Add hardening bindnow to rules file.
  * Release to unstable.

 -- Scarlett Moore <sgmoore@kde.org>  Fri, 13 Sep 2019 05:30:55 -0700

keurocalc (1.2.3-3) unstable; urgency=medium

  * Team upload.
  * Switch Homepage to https://www.kde.org/applications/office/keurocalc/,
    as the former website does not exist anymore.
  * Bump the debhelper compatibility to 11:
    - bump compat to 11
    - bump the debhelper build dependency to 11
  * Pass --fail-missing to dh_missing, to catch uninstalled files.
  * Tighten the keurocalc-data dependency in keurocalc.
  * Drop the provided man page for keurocalc: keurocalc has no command line
    arguments other than --help/--version/--authors, and it has no non-GUI
    workflow.
  * Bump Standards-Version to 4.2.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Sat, 06 Oct 2018 12:49:28 +0200

keurocalc (1.2.3-2) unstable; urgency=medium

  * Team upload.
  * Switch Vcs-* fields to salsa.debian.org.
  * Remove trailing whitespaces in changelog, and control.
  * Drop menu file, since keurocalc already provides a .desktop file.
  * Bump the debhelper compatibility to 10:
    - bump the debhelper build dependency to 10~
    - bump compat to 10
  * Remove debian/tmp prefixes from install files.
  * Use the icons already correctly installed, instead of copying them manually
    from the source directory
    - drop debian/dirs, no more needed now
  * Rewrite rules to use the dh sequencer:
    - drop the cdbs build dependency
    - add the pkg-kde-tools build dependency
    - use the kde dh addon
    - shuffle which docs are installed:
      - install nothing in keurocalc-data, since it is not needed there
      - stop installing TODO, since it is not useful for users
      - stop installing README, since it is empty
  * Remove the unused ${shlibs:Depends} substvar in keurocalc-data.
  * Link in as-needed mode.
  * Move the icons from keurocalc to keurocalc-data
    - add proper breaks/replaces
  * Bump Standards-Version to 4.1.4, no more changes required now.

 -- Pino Toscano <pino@debian.org>  Tue, 05 Jun 2018 11:31:02 +0200

keurocalc (1.2.3-1) unstable; urgency=medium

  * Imported Upstream version 1.2.3
  * Removed policy patch, applied upstream.
  * Bumped Standards-Version to 3.9.6, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Fri, 27 Feb 2015 13:19:27 +0100

keurocalc (1.2.2-1) unstable; urgency=medium

  * Imported Upstream version 1.2.2
  * Added patch to use old cmake behaviour for policy 0002.
  * Updated control file for latest policy changes.

 -- Michael Meskes <meskes@debian.org>  Thu, 14 Aug 2014 17:37:44 +0200

keurocalc (1.2.0-1) unstable; urgency=low

  * Fixed syntax in local-options file.
  * Imported Upstream version 1.2.0
  * Fixed description to list correct sources of exchange rates downloaded.
  * Bumped Standards-Version to 3.9.3, no changes needed.
  * Slightly updated copyright file.

 -- Michael Meskes <meskes@debian.org>  Wed, 29 Feb 2012 12:37:49 +0100

keurocalc (1.1.0-1) unstable; urgency=low

  * Imported Upstream version 1.1.0
  * Bumped Standards-Version to 3.9.1, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Sun, 23 Jan 2011 17:11:30 +0100

keurocalc (1.0.3-2) unstable; urgency=low

  * Rename /usr/share/doc/kde4 to /usr/share/doc/kde. (Closes: #584382)

 -- Michael Meskes <meskes@debian.org>  Thu, 03 Jun 2010 16:15:46 +0200

keurocalc (1.0.3-1) unstable; urgency=low

  * Imported Upstream version 1.0.3

 -- Michael Meskes <meskes@debian.org>  Mon, 31 May 2010 12:36:06 +0200

keurocalc (1.0.2-3) unstable; urgency=low

  * Added cmake build dependency which is no longer provided by
    kdelibs5-dev.
  * Bumped Standards-Version to 3.8.4, no changes needed.
  * Added source/format file.
  * Updated copright notice.

 -- Michael Meskes <meskes@debian.org>  Thu, 06 May 2010 11:01:20 +0200

keurocalc (1.0.2-2) unstable; urgency=low

  * Released to unstable.
  * Bumped Standards-Version to 3.8.1, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Tue, 07 Apr 2009 13:08:55 +0200

keurocalc (1.0.2-1) experimental; urgency=low

  * New upstream version.

 -- Michael Meskes <meskes@debian.org>  Mon, 12 Jan 2009 12:56:30 +0100

keurocalc (1.0.1-2) experimental; urgency=low

  * Added missing icon files.

 -- Michael Meskes <meskes@debian.org>  Mon, 05 Jan 2009 12:24:51 +0100

keurocalc (1.0.1-1) experimental; urgency=low

  * New upstream version for KDE4.
  * Removed patches that were not needed anymore and simplified packaging.
  * Migrated to cdbs.

 -- Michael Meskes <meskes@debian.org>  Fri, 02 Jan 2009 20:34:52 +0100

keurocalc (0.9.7-1) unstable; urgency=low

  * New maintainer, closes: #484541
  * Updated standards to 3.8.0
  * New upstream version
  * Removed relibtoolize patch
  * Added patch to update Makefile.in files.
  * Do not build depend on autotools-dev
  * Fixed watch file, closes: #450111
  * Downgraded recommendation on keurocalc in data package to a suggestion,
    closes: #378609
  * Adusted menu file to new structure.
  * keurocalc-data now replaces keurocalc, closes: #373149
  * Do not call configure twice.

 -- Michael Meskes <meskes@debian.org>  Fri, 06 Jun 2008 11:27:07 +0200

keurocalc (0.9.6-1) unstable; urgency=low

  * New Upstream Release
  * Split into a binary-independent data package and a binary package
  * Adjusted the relibtoolize patch
  * Upgrade debhelper compatibility level to 5
  * Bump Standards-Version to 3.7.2

 -- Claudio Moratti <maxer@knio.it>  Mon, 15 May 2006 12:04:01 +0200

keurocalc (0.9.4-5) unstable; urgency=low

  * Added Patch for libtool
  * Added a dh_desktop call
  * Fixed the category tag in .desktop file
  * Added debian/watch file

 -- Claudio Moratti <maxer@knio.it>  Sat,  3 Dec 2005 15:45:01 +0100

keurocalc (0.9.4-4) unstable; urgency=low

  * Fixed a bug in po/Makefile that adds a Makefile in .diff file

 -- Claudio Moratti <maxer@knio.it>  Sun, 18 Sep 2005 01:27:34 +0200

keurocalc (0.9.4-3) unstable; urgency=low

  * Package rebuilt from scratch, because of an missing Makefile
  * Changed license of manpage, from GFDL to GPL
  * Added an lintian override file for a source-contains-cvs-conflict-copy
    error
  * Added keurocalc homepage in long description
  * Changed Free Software Foundation address

 -- Claudio Moratti <maxer@knio.it>  Sat, 10 Sep 2005 23:05:31 +0200

keurocalc (0.9.4-2) unstable; urgency=low

  * Rebuilt after C++ ABI transition

 -- Claudio Moratti <maxer@knio.it>  Sat, 10 Sep 2005 00:38:59 +0200

keurocalc (0.9.4-1) unstable; urgency=low

  * New Upstream Release

 -- Claudio Moratti <maxer@knio.it>  Sat, 10 Sep 2005 00:20:31 +0200

keurocalc (0.9.2-1) unstable; urgency=low

  * Closed ITP bug for wnpp, Closes: #309782
  * New upstream release

 -- Claudio Moratti <maxer@knio.it>  Thu,  2 Jun 2005 11:14:35 +0200

keurocalc (0.9.1-1) unstable; urgency=low

  * Initial Release.

 -- Claudio Moratti <maxer@knio.it>  Thu, 12 May 2005 20:09:51 +0200
